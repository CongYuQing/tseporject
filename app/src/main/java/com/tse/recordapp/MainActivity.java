package com.tse.recordapp;

import android.content.DialogInterface;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.tse.recordapp.Database.ProductDao;
import com.tse.recordapp.Database.ProductDataBase;
import com.tse.recordapp.Database.ProductEntity;
import com.tse.recordapp.ui.home.HomeFragment;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;
import androidx.navigation.ui.AppBarConfiguration;
import androidx.navigation.ui.NavigationUI;
import androidx.room.Room;

import org.w3c.dom.Text;

import java.lang.reflect.Field;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import javax.xml.transform.Result;

public class MainActivity extends AppCompatActivity {
    // 若500ms之内无字符输入，则表示扫码完成. (若觉得时间还长，则可以设置成更小的值)
    private final static long MESSAGE_DELAY = 800;
    private StringBuilder mResult = new StringBuilder();//扫码内容
    private Handler mHandler = new Handler();
    private boolean mCaps;//大写或小写
    private ProductDataBase dataBase;
    private ProductDao productDao;
    private EditText editText;

    /* 扫描数据完成 */
    private final Runnable mScanningEndRunnable = new Runnable() {
        @Override
        public void run() {
            if (editText.getText().length() > 4){
                String productId = editText.getText().toString().trim();
                //长度大于4，代表扫描成功
                //1.查询数据库是否存在
                queryProduct(productId,true);
            }
            editText.setText("");
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        BottomNavigationView navView = findViewById(R.id.nav_view);
        // Passing each menu ID as a set of Ids because each
        // menu should be considered as top level destinations.
        AppBarConfiguration appBarConfiguration = new AppBarConfiguration.Builder(
                R.id.navigation_home, R.id.navigation_search)
                .build();
        NavController navController = Navigation.findNavController(this, R.id.nav_host_fragment);
        NavigationUI.setupActionBarWithNavController(this, navController, appBarConfiguration);
        NavigationUI.setupWithNavController(navView, navController);
        productDao = ProductDataBase.getDatabase(MainActivity.this).productDao();
    }

    @Override
    protected void onStart() {
        super.onStart();
        editText = findViewById(R.id.scanEditText);
    }

    /* 显示弹窗*/
    private void showAlerDialog(String alertText) {
        Looper.prepare();
        AlertDialog dialog = new AlertDialog.Builder(this)
                .setMessage(alertText)
                .setPositiveButton("确定",null)
                .create();
        dialog.show();
        dialog.getButton(AlertDialog.BUTTON_POSITIVE).setTextSize(28);
        try {
            //获取mAlert对象
            Field mAlert = AlertDialog.class.getDeclaredField("mAlert");
            mAlert.setAccessible(true);
            Object mAlertController = mAlert.get(dialog);

            //获取mMessageView并设置大小颜色
            assert mAlertController != null;
            Field mMessage = mAlertController.getClass().getDeclaredField("mMessageView");
            mMessage.setAccessible(true);
            TextView mMessageView = (TextView) mMessage.get(mAlertController);
            assert mMessageView != null;
            mMessageView.setTextSize(28);
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (NoSuchFieldException e) {
            e.printStackTrace();
        }
        Looper.loop();
    }


    /*键盘输入监听方法*/
    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN_MR1)
    @Override
    public boolean dispatchKeyEvent(KeyEvent event) {
        int keyCode = event.getKeyCode();
        Log.e("dispatchKeyEvent", "eventAction" + event.getAction());
        if (event.getAction() == KeyEvent.ACTION_DOWN) {
            if (keyCode == KeyEvent.KEYCODE_ENTER) {
                //若为回车键，直接返回
                mHandler.removeCallbacks(mScanningEndRunnable);
                mHandler.post(mScanningEndRunnable);
            } else {
                //延迟post，若500ms内，有其他事件
                mHandler.removeCallbacks(mScanningEndRunnable);
                mHandler.postDelayed(mScanningEndRunnable, MESSAGE_DELAY);
            }
        }
        return super.dispatchKeyEvent(event);
    }


    private boolean isInHomeFragment(){
        Fragment hostFragment =  MainActivity.this.getSupportFragmentManager().findFragmentById(R.id.nav_host_fragment);
        assert hostFragment != null;
        Fragment frag = hostFragment.getChildFragmentManager().getPrimaryNavigationFragment();
        assert frag != null;
        return (frag.getClass() == HomeFragment.class);
    }


    /* 插入数据 */
    private  void insertProduct(ProductEntity ...productEntities){
        new Thread(){
            @Override
            public void run() {
                productDao.insert(productEntities);
                TextView textView = findViewById(R.id.scanTextView);
                textView.post(() -> textView.setText("编号"+ productEntities[0].getProductId() + "录入成功！使用次数："+productEntities[0].getUseTimes()));
            }
        }.start();
    }

    /* 查询某一条数据 */
    private  void queryProduct(String productId,boolean isInScanFragment){
        new Thread(){
            @Override
            public void run() {
                List<ProductEntity> list = productDao.queryProduct(productId);
                if (isInScanFragment){
                    //1. 扫描结束后的查询
                    if (list.size() > 0){
                        ProductEntity product = list.get(0);
                        boolean isInTwoHours = isInTwoHours(product.getTimpStamp());
                        int useTimes = product.getUseTimes();
                        // 是否使用超过200次
                        if (useTimes > 200){
                            showAlerDialog("编号" + productId + "已经使用超过200次！！请及时更换");
                            return;
                        }
                        // 是否间隔在两小时以内
                        if (isInTwoHours){
                            showAlerDialog("编号" + productId + "2小时内已经记录，2小时内无法重复记录");
                            return;
                        }
                        // 存储并且显示
                        product.setTimpStamp(new Date().getTime());
                        product.setUseTimes(useTimes + 1);
                        insertProduct(product);
                    }else {
                        //没有查询到，直接存储
                        ProductEntity newProduct =  new ProductEntity(productId,1,new Date().getTime());
                        insertProduct(newProduct);
                    }

                }else{
                    //2. 查询页面时的查询
                }
            }
        }.start();
    }

    /* 查询所有数据 */
    private  void queryAllProducts(){
        new Thread(){
            @Override
            public void run() {
                productDao.getAllProducts();

            }
        }.start();
    }

    /*是否在两小时以内*/
    public  static  boolean isInTwoHours(long timstamp){
        long currentTimeStamp = new Date().getTime();
        long twoHour = 1000 * 60 * 60 * 2;
        return (timstamp + twoHour > currentTimeStamp);
    }

}

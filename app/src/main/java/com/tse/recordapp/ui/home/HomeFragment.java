package com.tse.recordapp.ui.home;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;

import com.tse.recordapp.R;

public class HomeFragment extends Fragment {

    private HomeViewModel homeViewModel;

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        homeViewModel =
                new ViewModelProvider(this).get(HomeViewModel.class);
        View root = inflater.inflate(R.layout.fragment_scan, container, false);

        // 每次进入获取焦点
        EditText text = root.findViewById(R.id.scanEditText);
        text.setFocusable(true);
        text.setFocusableInTouchMode(true);
        text.requestFocus();
        // 设置隐藏
//        text.setVisibility(View.INVISIBLE);
        return root;
    }

}
package com.tse.recordapp.Database;

import android.content.Context;

import androidx.annotation.NonNull;
import androidx.room.Database;
import androidx.room.DatabaseConfiguration;
import androidx.room.InvalidationTracker;
import androidx.room.Room;
import androidx.room.RoomDatabase;
import androidx.sqlite.db.SupportSQLiteOpenHelper;

@Database(entities = {ProductEntity.class},version = 1,exportSchema = false)
public abstract class ProductDataBase extends RoomDatabase {
   public abstract ProductDao productDao();
   private static ProductDataBase INSTANCE;
   public static synchronized ProductDataBase getDatabase(Context context){
       if (INSTANCE == null){
           INSTANCE = Room.databaseBuilder(context.getApplicationContext(),ProductDataBase.class,"Product_database").build();
       }
       return INSTANCE;
    }
}

package com.tse.recordapp.Database;

import androidx.annotation.NonNull;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

@Entity(tableName = "productTable")
public class ProductEntity {
    @PrimaryKey
    @NonNull
    private String productId;// 产品id
    private int useTimes;//使用次数
    private long timpStamp;//最近一次使用时间戳

     public ProductEntity(String productId, int useTimes, long timpStamp){
         this.productId = productId;
         this.useTimes = useTimes;
         this.timpStamp = timpStamp;
     }
    @NonNull
    public String getProductId() {
        return this.productId;
    }

    public void setProductId(@NonNull String productId) {
        this.productId = productId;
    }

    public int getUseTimes() {
        return this.useTimes;
    }

    public void setUseTimes(int useTimes) {
        this.useTimes = useTimes;
    }

    public void setTimpStamp(long timpStamp) {
        this.timpStamp = timpStamp;
    }

    public long getTimpStamp() {
        return timpStamp;
    }
}

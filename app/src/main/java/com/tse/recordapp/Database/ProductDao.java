package com.tse.recordapp.Database;

import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;

import java.util.List;

@Dao
public interface ProductDao {

    // 查询所有数据
    @Query("SELECT * FROM productTable")
    public List<ProductEntity> getAllProducts();

    // 查询某条数据
    @Query("SELECT * FROM productTable WHERE productId =:productId")
    public  List<ProductEntity> queryProduct(String productId);

    // 插入一条数据
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insert(ProductEntity ...product);
}
